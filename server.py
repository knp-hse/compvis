import socket
import numpy as np
from multiprocessing import Process
from io import BytesIO
from improcess import *


def user_handler(sock, dictpath, nnpath, clearim, size):
    file = BytesIO()
    data = sock.recv(1024)
    while data:
        file.write(data)
        data = sock.recv(1024)
    file.seek(0)
    try:
        ans = str.encode(immain(np.array(Image.open(file).convert('L')), dictpath, nnpath, clearim, size))
    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        ans = str.encode('ERROR')
    sock.send(ans)
    sock.close()


def serv(port, dictpath, nnpath, clearim, size):
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind((socket.gethostname(), port))
    serversocket.listen(10)

    while True:
        (clientsocket, address) = serversocket.accept()
        p = Process(target=user_handler, args=(clientsocket, dictpath, nnpath, clearim, size))
        p.run()
