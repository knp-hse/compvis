import sys
import pickle
import configparser
from server import *


def main():
    if len(sys.argv) < 2:
        print('Not enough args, try help')

    config = configparser.ConfigParser()
    config.read('config.ini')

    if sys.argv[1] == 'image':
        if len(sys.argv) < 4:
            print('Not enough args, try help')
            return
        if sys.argv[2] != 'real' and sys.argv[2] != 'clear':
            print("Incorrect mode, try help")
            return
        print(immain(np.array(Image.open(sys.argv[3]).convert('L')), config['prediction']['dict'],
              config['prediction']['nn'], sys.argv[2] == 'real', int(config['prediction']['size'])))

    elif sys.argv[1] == 'server':
        if len(sys.argv) < 3:
            print('Not enough args, try help')
            return
        if sys.argv[2] != 'real' and sys.argv[2] != 'clear':
            print("Incorrect mode, try help")
            return
        serv(int(config['network']['port']), config['prediction']['dict'], config['prediction']['nn'],
             sys.argv[2] == 'real', int(config['prediction']['size']))

    elif sys.argv[1] == 'genim':
        genimages(config['learning']['CROHME'], config['learning']['imspath'], int(config['prediction']['size']))

    elif sys.argv[1] == 'genset':
        gendataset(config['learning']['imspath'], config['learning']['dataset'])

    elif sys.argv[1] == 'learn':
        di = lenet_learn(config['learning']['dataset'], config['prediction']['nn'], int(config['prediction']['size']),
                         int(config['learning']['epochs']))
        di = {v: k for (k, v) in di.items()}
        with open(config['prediction']['dict'], 'wb') as fp:
            pickle.dump(di, fp)

    elif sys.argv[1] == 'help':
        print('Usage:\n'
              'image MODE IMAGE - predict symbols from IMAGE. Parameters are specified in config file. Modes:\n\t'
              'clear - b&w image\n\t'
              'real - anything else\n\n'
              'server MODE - run as a server listening to port from config.\n\n'
              'genim - create images of symbols from CROHME dataset using paths from config\n\n'
              'genset - create dataset from images using paths from config\n\n'
              'learn - create neural network using paths from config\n\n'
              'help - display this message\n\n')
    else:
        print('Unknown command, try help')


if __name__ == '__main__':
    main()
