from PIL import Image
from skimage.morphology import rectangle
from scipy.ndimage import filters
from skimage.filters import rank
import numpy as np


def imresize(ar, imsize):
    im = Image.fromarray(ar)
    return np.array(im.resize(imsize))


def histeq(im, nbr_bins=256):
    imhist, bins = np.histogram(im.flatten(), nbr_bins, normed=True)
    cdf = imhist.cumsum()
    cdf = 255 * cdf / cdf[-1]

    im2 = np.interp(im.flatten(), bins[:-1], cdf)
    return im2.reshape(im.shape)


def otsu(im):
    hist, bins = np.histogram(im.flatten(), 256)
    sum_all = 0
    for i in range(256):
        sum_all += i * hist[i]
    sum_back = 0
    w_back = 0
    var_max = 0
    threshold = 0
    total = np.size(im)

    for t in range(256):
        w_back += hist[t]
        if w_back == 0:
            continue
        w_fore = total - w_back
        if w_fore == 0:
            continue

        sum_back += t * hist[t]
        mean_back = sum_back / w_back
        mean_fore = (sum_all - sum_back) / w_fore

        var_between = w_back * w_fore * (mean_back - mean_fore) ** 2

        if var_between > var_max:
            var_max = var_between
            threshold = t

    return threshold


def tobinary(im, th):
    for i in range(np.size(im, 0)):
        for j in range(np.size(im, 1)):
            if im[i][j] >= th:
                im[i][j] = 255
            else:
                im[i][j] = 0


def apply_mask_im(im, mask, radius):
    h = np.size(im, 0)
    w = np.size(im, 1)
    ans = np.full(im.shape, 255)
    for i in range(h):
        for j in range(w):
            if mask[i][j] == 0:
                for diff1 in range(2*radius):
                    newi = i - radius + 1 + diff1
                    if not 0 <= newi < h:
                        continue
                    for diff2 in range(2*radius):
                        newj = j - radius + 1 + diff2
                        if 0 <= newj < w:
                            ans[newi][newj] = im[newi][newj]
    return ans


def mark_connected(im, checked, x, y, ans):
    if im[x][y] == 0 or checked[x][y] != 0:
        return
    checked[x][y] = 1
    ans[-1].append(y)
    ans[-1].append(x)
    mark_connected(im, checked, x - 1, y, ans)
    mark_connected(im, checked, x + 1, y, ans)
    mark_connected(im, checked, x, y - 1, ans)
    mark_connected(im, checked, x, y + 1, ans)


def divide_by_connectivity(im):
    checked = np.zeros(im.shape)
    ans = []
    for i in range(np.size(im, 0)):
        for j in range(np.size(im, 1)):
            if im[i][j] != 0 and checked[i][j] == 0:
                ans.append([])
                mark_connected(im, checked, i, j, ans)
    return ans


def clear_im(im):
    imx = np.zeros(im.shape)
    filters.sobel(im, 1, imx)
    imy = np.zeros(im.shape)
    filters.sobel(im, 0, imy)
    magnitude = np.sqrt(imx ** 2 + imy ** 2)
    xx = max(magnitude.flatten())
    magnitude = -1 * np.uint8(255 * magnitude / xx) + 255

    ot = otsu(magnitude)
    magnitude = 255 * (magnitude > ot)
    im = apply_mask_im(im, magnitude, 10)

    ans = np.array(im)

    for i in range(np.size(im, 0)):
        for j in range(np.size(im, 1)):
            if im[i][j] == 255:
                ans[i][j] = 0

    local_otsu = rank.otsu(ans, rectangle(21, 21))
    ans = 255 * (ans >= local_otsu)

    for i in range(np.size(im, 0)):
        for j in range(np.size(im, 1)):
            if im[i][j] == 255:
                ans[i][j] = 255
    return ans
