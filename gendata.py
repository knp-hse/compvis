from PIL import Image, ImageDraw
from xml.etree import ElementTree
import numpy as np
import os


def remove_namespace(doc, namespace):
    ns = u'{%s}' % namespace
    nsl = len(ns)
    for elem in doc.getiterator():
        if elem.tag.startswith(ns):
            elem.tag = elem.tag[nsl:]


def genimage(linelist, size):
    im = Image.new('RGB', (size, size), 0xFFFFFF)
    draw = ImageDraw.Draw(im)
    for coords in linelist:
        draw.line(coords, fill=0x0, width=int(size/17))
    return im


def genimages(datapath, outpath,  size):
    for root, dirs, files in os.walk(datapath):
        symdict = {}
        for filename in files:
            if filename.endswith(".inkml"):
                e = ElementTree.parse(root + '/' + filename).getroot()
                remove_namespace(e, "http://www.w3.org/2003/InkML")

                characters = e.find('traceGroup')
                for char in characters.findall('traceGroup'):
                    symb = char.find('annotation')
                    symname = symb.text.replace('\\', '').replace('/', 'slash').replace('.', 'point')
                    symdict[symname] = symdict.get(symname, 0) + 1
                    lines = []
                    for t in char.findall('traceView'):
                        for trace in e.findall('trace'):
                            if t.get('traceDataRef') == trace.get('id'):
                                k = trace.text.split(',', 1)
                                newl = trace.text.replace(",", "").replace('\n', '').split(' ')
                                if k[0].count(" ") > 1:
                                    del newl[2::3]
                                lines.append(newl)

                    maxx = 0
                    minx = 1e10
                    maxy = 0
                    miny = 1e10
                    for i in range(len(lines)):
                        lines[i] = [float(n) for n in lines[i]]
                        if max(lines[i][::2]) > maxx:
                            maxx = max(lines[i][::2])
                        if min(lines[i][::2]) < minx:
                            minx = min(lines[i][::2])
                        if max(lines[i][1::2]) > maxy:
                            maxy = max(lines[i][1::2])
                        if min(lines[i][1::2]) < miny:
                            miny = min(lines[i][1::2])

                    if maxx-minx == maxy-miny == 0:
                        continue
                    if maxx - minx < maxy - miny:
                        mult = float(size) / (maxy - miny)
                        minx -= ((maxy - miny) - (maxx - minx))/2
                    else:
                        mult = float(size) / (maxx - minx)
                        miny -= ((maxx - minx) - (maxy - miny))/2
                    for i in range(len(lines)):
                        lines[i][::2] = [(n - minx) for n in lines[i][::2]]
                        lines[i][1::2] = [(n - miny) for n in lines[i][1::2]]
                        lines[i] = [int(n * mult) for n in lines[i]]
                    image = genimage(lines, size)
                    dst = outpath + '/' + symname
                    if not os.path.isdir(dst):
                        os.makedirs(dst)
                    image.save(dst + '/' + str(symdict[symname]) + '.png')


def gendataset(datapath, outputfile, flat=False):
    dataset_x = []
    dataset_y = []
    for root, dirs, files in os.walk(datapath):
        symname = root.rsplit("/", 1)[1]
        print(symname)
        for filename in files:
            if filename.endswith(".png"):
                dataset_y.append(symname)
                im = np.array(Image.open(root + '/' + filename).convert('L'))
                im = 1*(im < 128)
                if flat:
                    im = im.flatten()
                im = np.uint8(im)
                dataset_x.append(im)

    dataset_x = np.array(dataset_x)
    dataset_y = np.array(dataset_y)
    np.savez(outputfile, dataset_x=dataset_x, dataset_y=dataset_y)
